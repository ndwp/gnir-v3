# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
- [VS Code](https://cn.vitejs.dev/)

## 操作记录
npm init vite@latest

cd <pgname>

npm i

npm run dev

npm run build

npm run preview


npm i vue-router@4
npm i vuex@next
npm i axios
npm i vue-i18n@next
npm i @smallwei/avue@next
npm i less --save-dev

.gitignore 添加 package-lock.json 