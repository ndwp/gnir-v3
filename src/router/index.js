
import { createRouter, createWebHashHistory} from 'vue-router'
import localRouter from './routes'
// import autoRouter from "./auto-routes";
import generatedRoutes from 'virtual:generated-pages'
import { setupLayouts } from 'virtual:generated-layouts'
// 路由魔改 auto = false 不自动生成路由
// 路由魔改 layout = false 的组件不用布局
const autoRouter =[]
generatedRoutes.forEach(v => {
    if(v?.meta?.auto != false ) {
        autoRouter.push(v?.meta?.layout != false ? setupLayouts([v])[0] : v)
    }

})

console.log(autoRouter)
export const routes = [...localRouter,...autoRouter]

console.log(routes)
const router = createRouter({
    history: createWebHashHistory(),
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            if (from.meta.keepAlive) {
                from.meta.savedPosition = document.body.scrollTop;
            }
            return {
                x: 0,
                y: to.meta.savedPosition || 0,
            };
        }
    },
    routes
})


// export default router
export default function loadRouter(app) {
    // setStore({ name: "menu", content: routes })
    app.use(router);
}