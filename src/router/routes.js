import website from '@/config/website'

// 自定义的路由
const routes =[
    website.firstPage,
    {
        path: "/:cathchAll(.*)",
        redirect: "/error/404",
    },
]

export default routes;