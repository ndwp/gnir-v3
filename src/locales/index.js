import elementZhLocale from "element-plus/es/locale/lang/zh-cn"; // element-ui lang
import elementEnLocale from "element-plus/es/locale/lang/en"; // element-ui lang
import avueZhLocale from '@smallwei/avue/lib/locale/lang/zh'
import avueEnLocale from '@smallwei/avue/lib/locale/lang/en'
import enLocale from "./en";
import zhLocale from "./zh-cn";
const lang =  {
    en: {
        ...enLocale,
        ...elementEnLocale,
        ...avueEnLocale
    },
    zh: {
        ...zhLocale,
        ...elementZhLocale,
        ...avueZhLocale
    }
}
import { createI18n } from 'vue-i18n'



export const i18n = createI18n({
    locale: 'zh',
    messages: lang,
});

export default function loadI18n(app) {
    app.use(i18n);
}