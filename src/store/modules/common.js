const common = {
    namespaced: false,
    state: () => ({
        language: 'zh',
    }),
    mutations: {
        SET_LANGUAGE: (state, language) => {
            state.language = language
            // setStore({
            //     name: 'language',
            //     content: state.language
            // })
        },
    }
}
export default common