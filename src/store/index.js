import { createStore } from 'vuex'

// vuex 持久化 localStroe
import createPersistedState from 'vuex-persistedstate'

import getters from '@/store/getters'
import common from '@/store/modules/common'

export const store = createStore({


    getters,
    modules: {
        common
    },
    plugins: [
        createPersistedState({
                key: 'gnir-vuex-client',
                paths: ['common']
            })
    ],
})

export default function loadVuex(app) {
    app.use(store)
}