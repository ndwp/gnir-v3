export default {
    column: [
      {
        type: 'input',
        label: '项目名称',
        span: 24,
        display: true,
        prop: 'a167056541866353509',
        required: true,
        rules: [
          {
            required: true,
            message: '项目名称必须填写'
          }
        ]
      },
      {
        type: 'radio',
        label: '是否包含子模块',
        dicData: [
          {
            label: '否',
            value: 'false'
          },
          {
            label: '是',
            value: 'true'
          }
        ],
        span: 24,
        display: true,
        props: {
          label: 'label',
          value: 'value'
        },
        prop: 'a167056586435967697',
        required: true,
        rules: [
          {
            required: true,
            message: '是否包含子模块必须填写'
          }
        ]
      }
    ],
    labelPosition: 'left',
    labelSuffix: '：',
    labelWidth: 120,
    gutter: 0,
    menuBtn: true,
    submitBtn: true,
    submitText: '提交',
    emptyBtn: true,
    emptyText: '清空',
    menuPosition: 'center'
  }