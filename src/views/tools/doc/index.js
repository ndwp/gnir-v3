import {ref, reactive} from 'vue'

const data = reactive({
    treeData: [],
    form: {}
  })
export const handleSubmit = (val) =>{
    this.$message.success("查看控制台")
    console.log(val);
    this.option = val;
}



export const handleWebapiJson =(tags,paths) => {
    Object.keys(paths).forEach(path=> {
        Object.keys(paths[path]).forEach(type=> {
          paths[path][type].tags.forEach(tag=> {
            let tmptag = tags.find(x=> x.name===tag)
            if(!tmptag.children) {
              tmptag.children =[]
            }
            let {summary} = paths[path][type]
            tmptag.children.push({name: summary,path,type,...paths[path][type]})
          })
        })
      })
  }