import request from "@/router/request";
// import { baseUrl } from "@/config/env";
const baseUrl ="http://localhost:8080/simple"
export const getV2Doc = () => {
    return request({
        url: baseUrl+"/v2/api-docs",
        method: "get",
    });
}