export const formOption =  {
    column: [
      {
        type: 'title',
        span: 24,
        display: true,
        styles: {
          fontSize: '18px',
          color: '#000'
        },
        labelWidth: '0px',
        value: '接口信息',
        prop: 'name'
      },
      {
        type: 'input',
        label: '接口地址',
        span: 24,
        display: true,
        prop: 'path',
        append: '调试'
      },
      {
        type: 'input',
        label: '接口说明',
        span: 24,
        display: true,
        prop: 'description'
      },
      {
        type: 'input',
        label: '请求数据类型',
        span: 12,
        display: true,
        prop: 'consumes'
      },
      {
        type: 'input',
        label: '响应数据类型',
        span: 12,
        display: true,
        prop: 'produces'
      },
      {
        type: 'dynamic',
        label: '请求参数',
        span: 24,
        display: true,
        children: {
          align: 'center',
          headerAlign: 'center',
          index: false,
          addBtn: true,
          delBtn: true,
          column: [
            {
              label: '参数名称',
              prop: 'name',
              type: 'input',
              cell: true
            },
            {
              label: '参数说明',
              prop: 'description',
              type: 'input',
              cell: true
            },
            {
              label: '请求类型',
              prop: 'in',
              type: 'input',
              cell: true
            },
            {
              label: '是否必须',
              prop: 'required',
              type: 'input',
              cell: true
            },
            {
              label: '数据类型',
              prop: 'type',
              type: 'input',
              cell: true
            },
            {
              label: 'schema',
              prop: 'originalRef',
              type: 'input',
              cell: true
            }
          ]
        },
        prop: 'parameters'
      },
      {
        type: 'dynamic',
        label: '响应状态',
        span: 24,
        display: true,
        children: {
          align: 'center',
          headerAlign: 'center',
          index: false,
          addBtn: true,
          delBtn: true,
          column: [
            {
              label: '状态码',
              prop: 'code',
              type: 'input',
              cell: true
            },
            {
              label: '说明',
              prop: 'description',
              type: 'input',
              cell: true
            },
            {
              label: 'schema',
              prop: 'originalRef',
              type: 'input',
              cell: true
            }
          ]
        },
        prop: 'responses'
      },
      {
        type: 'dynamic',
        label: '响应参数',
        span: 24,
        display: true,
        children: {
          align: 'center',
          headerAlign: 'center',
          index: false,
          addBtn: true,
          delBtn: true,
          column: [
            {
              label: '参数名称',
              prop: 'code',
              type: 'input',
              cell: true
            },
            {
              label: '参数说明',
              prop: 'description',
              type: 'input',
              cell: true
            },
            {
              label: '类型',
              prop: 'type',
              type: 'input',
              cell: true
            },
            {
              label: 'schema',
              type: 'input',
              prop: 'originalRef',
              cell: true
            }
          ]
        },
        prop: 'success'
      }
    ],
    labelPosition: 'left',
    labelSuffix: '：',
    labelWidth: 120,
    gutter: 0,
    menuBtn: true,
    submitBtn: true,
    submitText: '提交',
    emptyBtn: false,
    emptyText: '清空',
    menuPosition: 'center',
    detail: false
  }
  export const treeOption = {  
    defaultExpandAll: false,
    formOption:{
      labelWidth:100,
      column:[{
          label:'自定义项',
          prop:'name'
      }],
    },
    props:{
      labelText:'标题',
      label:'name',
      value:'value',
      children:'children'
    }
  }
