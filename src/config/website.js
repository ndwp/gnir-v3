// import {env} from "./env";

/**
 * 全局配置文件
 */
export default {
    key: "gnir-v3", //配置主键,目前用于存储
    title: "Vite3 Vue3 App",
    // 请求认证配置
    request: {
        basicKey: "Authorization",
        bearerKey: "ndwp-auth",
        basicPrefix: "Basic",
        bearerPrefix: "Bearer",
        clientId: "web", // 客户端id
        clientSecret: "web@123", // 客户端密钥
        statusWhiteList: [], // 请求状态白名单
    },
    // 首页配置
    firstPage: {
        path: "/",
        name: "首页",
        redirect: "/wel",
    },
    //配置菜单的属性
    menu: {
        pathWhiteList: [],
        iconDefault: "iconfont icon-caidan",
        props: {
            label: "name",
            path: "path",
            icon: "source",
            children: "children",
        },
    },
};
