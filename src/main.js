import { createApp } from 'vue'
import '@/assets/style/style.css'
import App from '@/App.vue'

const app = createApp(App)

// vue-router@next
import loadRouter from '@/router'
loadRouter(app)

// vuex@next
import loadStore from '@/store'
loadStore(app)

// ElementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
app.use(ElementPlus)

// @smallwei/avue@next
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
app.use(Avue);
// 使用字典和上传组件需要引入axios
// window.axios = axios;
// app.use(Avue,{axios})
// avue 表单设计器
import AvueFormDesign from '@sscfaith/avue-form-design'
app.use(AvueFormDesign)

// 国际化（其中引入了引入了其他插件的国际化）
import loadI18n  from '@/locales'
loadI18n(app)
// 等同于
// import {i18n}  from '@/locales'
// app.use(i18n)

//自动注册 components
const components = import.meta.glob('@/components/**/**.vue',{ eager: true })
Object.entries(components).forEach(([path, definition]) => {
    // Get name of component, based on filename
    // "./components/Fruits.vue" will become "Fruits"
    const componentName = path.split('/').pop().replace(/\.\w+$/, '')
    console.log(componentName)
    // Register component on this Vue instance
    app.component(componentName, definition.default)
})

app.mount('#app')