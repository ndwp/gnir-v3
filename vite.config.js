import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Pages from "vite-plugin-pages"
import Layouts from "vite-plugin-vue-layouts"
import path from 'path'
const resolve = (dir) => path.resolve(__dirname, dir)
export default defineConfig({
  plugins: [vue(),
    Pages({
      extensions: ['vue'],
      exclude:["**/components/**.vue"],
      dirs: [{dir:"src/views",baseRoute:""}],
      importMode: "async"
      // extendRoute(route, parent) {
      //   if (route.path === "/") {
      //     // Index is unauthenticated.
      //     return route;
      //   }
      //   // Augment the route with meta that indicates that the route requires authentication.
      //   return {
      //     ...route,
      //     meta: { isAuth: true },
      //   }}
    }),
      Layouts({
        layoutsDirs: 'src/layouts',
        defaultLayout: 'index'
      })
  ],
  resolve: {
    alias: {
      '@': resolve('src'),
    }
  },
  css: {
    // css预处理器
    preprocessorOptions: {
      less: {
        // 引入 mixin.scss 这样就可以在全局中使用 mixin.scss中预定义的变量了
        // 给导入的路径最后加上 ;
        javascriptEnabled: true,
        additionalData: '@import "@/assets/style/mixin.less";'
      }
    }
  },
  base:'/gnir-v3/',
  server: {
    host: 'localhost',
    port:3000,
    open: true
  }
})
